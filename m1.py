#!/usr/bin/env python
"""
Same as earlier variations, except calculate the pixels
once and just cycle them through the array instead of
recalculating anything
"""
from __future__ import division, print_function # inching towards py3k

import time
from collections import defaultdict

import opc

from color import RGB, HSV

def shift_l(arr):
    "mutate a list in place, shifting each elt left by one"
    x = arr.pop(0)
    arr.append(x)

def shift_r(arr):
    "mutate a list in place, shifting each elt right by one"
    x = arr.pop()
    arr.insert(0, x)

##
## Fadecandy physical layout
##
NUM_LEDS=64 # per port/strip

##
## Simplest possible model
##

class Pipe(object):
    def __init__(self, base, size=64):
        print("new Pipe[%d]" % base)
        self.base = base
        self.size = size
        self.pixels = [RGB(0,0,0) for x in range(self.size)]

    def off(self):
        "Turn all pixels off"
        for p in self.pixels:
            p.r = 0 # XXX make a convenience setter?
            p.g = 0
            p.b = 0

    def get_rgb_tuples(self):
        "Return a list of rgb tuples"
        return [pixel.rgb for pixel in self.pixels]

    def shift_l(self):
        "Shift all pixels 1 to the left, wrapping"
        shift_l(self.pixels)

    def shift_r(self):
        "Shift all pixels 1 to the right, wrapping"
        shift_r(self.pixels)

    def __len__(self):
        return self.size

class PipeModel(object):
    def __init__(self, client, ports=8):
        # for now just create one pipe per port, mapped starting
        # at input 0
        self.client = client
        self.numports = ports
        self.pipes = [Pipe(n * NUM_LEDS) for n in range(self.numports)]

    def set_all(self):
        # need to construct an array of all of the pipes
        dat = []
        for p in self.pipes:
            dat += p.get_rgb_tuples()
        # print("got %d pixels total to set" % len(dat))
        # print(dat)
        r = self.client.put_pixels(dat)
        if not r:
            # XXX what to do here?
            print("!!! error, pixels not set")
            raise Exception("!!!! pixels not set. fadecandy disconnected?")

    def reset(self):
        for p in self.pipes:
            p.off()

def getmillis():
    return time.time() * 1000

class Scene(object):
    def __init__(self, model):
        self.model = model

        self.animations = []

        self.ticks = 0
        self.fps = None # whether the scene is running

        # whether to periodically print status of running animations
        self.debug = False

        # XXX should probably use heapq here
        # key is an tick count of animation to be added
        self.delayed = defaultdict(list)

        # shows (or others) to notify when animations start & stop
        # this is a list of callables
        self.observers = []

    def _status(self):
        "return a string describing the number of running animations"
        return "[%d running %d queued]" % (len(self.animations), len(self.delayed))

    def is_running(self):
        return self.fps is not None

    ##
    ## animation management
    ##
    def add_animation(self, anim):
        "Immediately add an animation to a scene"
        self.animations.append(anim)
        print("added animation %s" % self._status())
        anim.start(getmillis())
        self._do_notification("start", anim)

    def schedule_animation(self, anim, tick_delay):
        "Add an animation to a scene after a delay"
        assert tick_delay > 0, "tick delay must be greater than zero"
        start_key = self.ticks + tick_delay
        self.delayed[start_key].append(anim)

    def remove_animation(self, anim):
        try:
            ix = self.animations.index(anim)
            del self.animations[ix]
            print("removed animation %d %s" % (ix, self._status()))
            self._do_notification("stop", anim)
        except ValueError:
            print("!!! trying to remove nonexistent animation")

    ##
    ## notification / observers
    ##
    def add_observer(self, observer):
        "Request callbacks when animations are descheduled"
        self.observers.append(observer)

    def _do_notification(self, action, anim):
        for o in self.observers:
            try:
                o(action, anim)
            except Exception, e:
                print("exception sending notification")
                print(e)

    ##
    ## animation run loop
    ##
    def next_frame(self):
        # run all current animations, descheduling any
        # that have run their course
        ms = getmillis()

        for anim in self.animations:
            if not anim.next_frame(ms):
                self.remove_animation(anim)

        # check if any scheduled animations need to start
        self.ticks += 1
        if self.ticks in self.delayed:
            for a in self.delayed[self.ticks]:
                print("starting delayed animation")
                self.add_animation(a)
            del self.delayed[self.ticks]

        # print status once a minute
        if self.debug and self.ticks % (self.fps * 60) == 0:
            print("*" * 80)
            print("current tick: %d" % self.ticks)
            print("%d running animations:" % len(self.animations))
            print(self.animations)
            print("delayed animations:")
            print(self.delayed.keys())
            print("*" * 80)

    def start(self, fps=30):
        self.fps = fps

        while self.is_running():
            self.next_frame()
            self.model.set_all()
            time.sleep(1/fps)

    def stop(self):
        "Cause 'start' to return at the end of next frame"
        # XXX have to call this from within a show for it to work
        self.fps = None

    def reset(self):
        "Remove all animations and clear the model"
        self.model.reset()

if __name__ == '__main__':
    client = opc.Client('localhost:7890')

    # model for pipes should be loaded from a mapping file
    PIPE_COUNT=8 # XXX from config
    model = PipeModel(client, ports=PIPE_COUNT)
    print(model)

    # create a single scene containing
    # 3 animations on the first pipe
    scene = Scene(model=model)

    # XXX load shows
    import shows
    show_map = dict(shows.load_shows())
    print("loaded shows:")
    print(show_map)

    # show = show_map['RandomPulse'](model=model, scene=scene, per_pipe=8)
    show = show_map['TestMapping'](model=model, scene=scene)
    print("starting show:%s" % show)

    # show = RandomPulse(model=model, scene=scene, per_pipe=8)
    # show = TestMapping(model, scene);

    # XXX think about how to rotate shows
    show.setup()

    # now run all the animations in the scene
    show_fps = show.fps if hasattr(show, 'fps') else 30
    scene.start(fps=show_fps)
