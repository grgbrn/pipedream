from __future__ import division, print_function # inching towards py3k

from math import exp, sin, pi, e
from color import RGB

# barely an animation at all...
class BlinkAnimation(object):
    def __init__(self, pixels, color, blink_count):
        self.pixels = pixels # list of pixels!
        self.color = color
        self.blink_count = blink_count

        assert len(pixels) > 0, "BlinkAnimation needs at least one pixel"

    def start(self, millis):
        self.ticks = 0

        self.fps = 30 / 2 # XXX
        self.onstate = False

    def next_frame(self, millis):
        self.ticks += 1
        if self.ticks % self.fps == 0:
            self.onstate = not self.onstate
            if self.onstate:
                self.set_pixels(self.color)
            else:
                self.set_pixels(RGB(0,0,0))
        return True

    def set_pixels(self, c):
        for p in self.pixels:
            # XXX add a primitive for this
            p.h = c.h
            p.s = c.s
            p.v = c.v


class ChaseAnimation(object):
    def __init__(self, pixels, color):
        self.pixels = pixels # list of pixels / 1 pipe
        self.color = color

        self.index = 0 # which pixel is illuminated
        self.dir = 1 # 1/-1 to indicate direction of travel

    def start(self, millis):
        pass

    def next_frame(self, millis):
        self.index += self.dir
        if self.index == 0:
            self.dir = 1
        elif self.index == len(self.pixels)-1:
            self.dir = -1

        self.set_pixels(RGB(0,0,0))
        p = self.pixels[self.index]
        p.h = self.color.h
        p.s = self.color.s
        p.v = self.color.v

        return True # keep running

    # XXX the pipe itself should have a primitive for this?
    def set_pixels(self, c):
        for p in self.pixels:
            # XXX add a primitive for this
            p.h = c.h
            p.s = c.s
            p.v = c.v


# simple breathe animation
# http://sean.voisen.org/blog/2011/10/breathing-led-with-arduino/

def breathe_intensity(millis):
    # output range int 0-255
    return int((exp(sin(millis/2000.0*pi)) - 0.36787944)*108.0)

class BreatheAnimation(object):
    def __init__(self, pixel, color, duration=None):
        self.pixel = pixel
        self.base_color = color
        self.max_ticks = duration

        # for correlating animations to the pipe that
        # owns their pixel (maybe not the best way to do this)
        self.tag = None

    def start(self, millis):
        "called when an animation is first added to a scene"
        self.ticks = 0
        self.start_millis = millis

    def next_frame(self, millis):
        # XXX document this
        t = millis - self.start_millis

        v255 = breathe_intensity(t)
        v = v255 / 255
        # print(v255, v)
        self.pixel.set_hsv(self.base_color.h, self.base_color.s, v)

        self.ticks += 1
        if self.max_ticks and self.ticks > self.max_ticks and v == 0:
            print("%s done!" % self)
            return False
        else:
            return True
