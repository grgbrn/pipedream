from random import randint

from color import RGB

class DripAnimation(object):
    def __init__(self, pixels, driphold):
        self.pixels = pixels # list of pixels!
        self.driphold = driphold #how long to hold the drip before it falls.
        
    def start(self, millis):
        self.ticks = 0   
        ## start light position     
        self.n = 0
        
        self.speed = 10

    def next_frame(self, millis):
        self.pixels[self.n].set_rgb(0,0,255)
        self.ticks += 1
        if self.ticks < self.driphold:
            return True
        ## n == current light position
        if self.n > 0:
            self.pixels[self.n-1].set_rgb(0,0,0)
        self.n += 1
        return self.n < len(self.pixels)

class Dripdown(object): # show
    def __init__(self, model, scene):
        self.model = model
        self.scene = scene
        
        self.fps = 100
        
        self.scene.add_observer(self.scene_notification)

    def setup(self):
        for (ix, pipe) in enumerate(self.model.pipes):
            print("starting animations on pipe %s" % pipe)
            a = DripAnimation(pipe.pixels, driphold=randint(100,500))
            self.scene.add_animation(a)

    def scene_notification(self, action, animation):
        # print("RandomShow got notification:%s for animation %s" % (action, animation))
        if action == 'stop':
            a = DripAnimation(animation.pixels, driphold=randint(100,500))
            self.scene.schedule_animation(a, randint(100,500))

