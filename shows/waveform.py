from __future__ import division, print_function # inching towards py3k

import randomcolor
from random import randint, randrange, choice

from color import HSV

import math
import time

from animations import BreatheAnimation

def frange(start, end, step=0.1):
    val = start
    while val < end:
        val += step
        val = round(val, 2)
        yield val

def sine_wave(n, period=1):
    # 1 period of a sine wave, in n discrete points
    step = (math.pi * 2 * period) / n
    return [math.sin(x) for x in frange(0.0, math.pi*2, step)]

class Waveform(object): # show
    def __init__(self, model, scene):
        self.model = model
        self.scene = scene

    def setup(self):
        for (ix, pipe) in enumerate(self.model.pipes):
            print("drawing on pipe %d [%s]" % (ix, pipe))

            base = self.random_color()

            vals = sine_wave(len(pipe)+2)
            pix = [HSV(base.h, base.s, max(0,v)) for v in vals]
            pipe.pixels = pix

        self.model.set_all()

        # now we actually run here in our own simple animation loop
        # even/odd numbered poles run in different speeds & directions
        tick = 0
        while True:
            time.sleep(1/30)
            tick += 1
            for (ix, p) in enumerate(self.model.pipes):
                if ix % 2 == 0:
                    if tick % 2 == 0: # half speed
                        p.shift_l()
                else:
                    p.shift_r()
            self.model.set_all()


    def random_color(self):
        "Return a HSV object with a color in an acceptable palette"
        basecolors = ['red', 'orange', 'yellow', 'pink']
        # XXX play with luminosity values
        # 'bright', 'light', 'dark', 'random'
        return randomcolor.random_color(choice(basecolors), luminosity='bright')
