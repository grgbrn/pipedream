from random import randint, randrange, choice
import randomcolor

from animations import BreatheAnimation

class Lustre(object): # show
    def __init__(self, model, scene, per_pipe=8):
        self.model = model
        self.scene = scene

        # number of pulses to schedule on each pipe
        self.n = per_pipe

        self.scene.add_observer(self.scene_notification)

    def setup(self):
        for (ix, pipe) in enumerate(self.model.pipes):
            print("starting animations on pipe %s" % pipe)

            for anim in range(self.n):
                self.new_animation(ix)

    # XXX
    def cleanup(self):
        self.scene.reset()

    def scene_notification(self, action, animation):
        # print("RandomShow got notification:%s for animation %s" % (action, animation))
        if action == 'stop':
            ix = animation.tag
            self.new_animation(ix)

    def new_animation(self, pipe_index):
        "Schedule a new random animation on a pipe"
        pipe = self.model.pipes[pipe_index]

        # pick a random pixel from the pipe, a random color
        # and a random duration
        pixel_ix = randrange(0, len(pipe))
        color = self.random_color()
        duration = randint(100,500)

        pixel = pipe.pixels[pixel_ix]

        a = BreatheAnimation(pixel, color=color, duration=duration)
        a.tag = pipe_index
        self.scene.schedule_animation(a, randint(10,100))

    def random_color(self):
        "Return a HSV object with a color in an acceptable palette"
        basecolors = ['red', 'orange', 'purple', 'pink']
        # XXX play with luminosity values
        # 'bright', 'light', 'dark', 'random'
        return randomcolor.random_color(choice(basecolors), luminosity='bright')
