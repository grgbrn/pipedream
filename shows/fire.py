"""
Based off Fire2012 by Mark Kriegsman, July 2012

https://pastebin.com/xYEpxqgq
https://www.tweaking4all.com/hardware/arduino/adruino-led-strip-effects/
https://www.youtube.com/watch?v=knWiGsmgycY&feature=youtu.be

"""

from math import exp, sin, pi, e
from color import RGB
from random import randint, randrange, choice

from animations import ChaseAnimation

def heat_to_pixel_fire(temp):
    "convert a heat value to a rgb tuple"
    # XXX temp is between 0-255?

    # scale down from 0-255 to 0-191
    t192 = int((temp/255.0) * 191)

    # calculate ramp up from
    heatramp = t192 & 0x3F # 0-63
    heatramp <<= 2 # scale up to 0-252

    # figure out which third of the spectrum we're in
    if t192 > 0x80: # hottest
        return (255,255,heatramp)
    elif t192 > 0x40: # middle
        return (255, heatramp, 0)
    else:
        return (heatramp, 0, 0)

def heat_to_pixel_eldritch(temp):
    "convert a heat value to a rgb tuple"
    # XXX temp is between 0-255?

    # scale down from 0-255 to 0-191
    t192 = int((temp/255.0) * 191)

    # calculate ramp up from
    heatramp = t192 & 0x3F # 0-63
    heatramp <<= 2 # scale up to 0-252

    # figure out which third of the spectrum we're in
    if t192 > 0x80: # hottest, white
        return (heatramp,255,255)
    elif t192 > 0x40: # middle
        return (0, heatramp, 255)
    else: # purple
        return (heatramp, 0, heatramp)


heat_to_pixel = heat_to_pixel_fire
# heat_to_pixel = heat_to_pixel_eldritch

class FlameAnimation(object):
    def __init__(self, pixels, pipesize=64):
        self.pixels = pixels # 1 pipe's worth of pixels
        self.heat = [0] * len(self.pixels)

        self.max = pipesize

        # animation variables
        self.cooling = 55
        self.sparking = 120
        self.speed_delay = 15


    def start(self, millis):
        self.set_pixels(RGB(0,0,0))

    def next_frame(self, millis):

        cooling_max = (self.cooling * 10) / len(self.pixels) + 2
        # print "cooling_max={}".format(cooling_max)

        # step 1, cool down every cell a little
        for (ix, heat) in enumerate(self.heat):

            # XXX this is a simple list comprehension and min() fn
            cooldown = randint(0, cooling_max)
            if cooldown > heat:
                self.heat[ix] = 0
            else:
                self.heat[ix] -= cooldown

        # h = [min(0, h - randint(0, cooling_max)) for h in self.heat]
        # print h

        # step 2, heat from each cell drifts up and diffuses a bit
        for ix in range(self.max-1,2,-1):
            # print ix, ix-1, ix-2, ix-3
            # print self.heat[ix-1], self.heat[ix-2], self.heat[ix-3]
            self.heat[ix] = (self.heat[ix-1] + self.heat[ix-2] + self.heat[ix-3]) / 3

        # step 3, randomly ignite new sparks near the bottom
        if randint(0,255) < self.sparking:
            y = randint(0,7)
            # print "spark! y={}".format(y)
            self.heat[y] = randint(160,255)

        # step 4, convert heat to RGB colors
        for (heat, pixel) in zip(self.heat, self.pixels):
            c = heat_to_pixel(heat)
            pixel.set_rgb(*c)

        # print self.heat


        """
        # self.ticks += 1
        # if self.ticks % self.fps == 0:
            # above this is just a ratelimit
        self.index += self.dir
        if self.index == 0:
            self.dir = 1
        elif self.index == len(self.pixels)-1:
            self.dir = -1

        self.set_pixels(RGB(0,0,0))
        p = self.pixels[self.index]
        p.h = self.color.h
        p.s = self.color.s
        p.v = self.color.v
        """

        return True # keep running

    # XXX the pipe itself should have a primitive for this?
    def set_pixels(self, c):
        for p in self.pixels:
            # XXX add a primitive for this
            p.h = c.h
            p.s = c.s
            p.v = c.v


class Fire(object): # show
    def __init__(self, model, scene):
        self.model = model
        self.scene = scene

        self.fps = 16

    def setup(self):
        for (ix, pipe) in enumerate(self.model.pipes):
            print("starting animations on pipe %s" % pipe)

            # a = ChaseAnimation(pipe.pixels, color=RGB(0,255,0))
            a = FlameAnimation(pipe.pixels)
            self.scene.add_animation(a)

    # XXX
    def cleanup(self):
        self.scene.reset()

    def scene_notification(self, action, animation):
        print("Fire got notification:%s for animation %s" % (action, animation))
