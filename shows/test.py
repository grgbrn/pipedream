from animations import BlinkAnimation

from color import RGB

class TestMapping(object): # show
    def __init__(self, model, scene):
        self.model = model
        self.scene = scene

    def setup(self):
        for (ix, pipe) in enumerate(self.model.pipes):
            print("starting animations on pipe %s" % pipe)
            pipenum = ix + 1
            pixels = pipe.pixels[0:pipenum]
            a = BlinkAnimation(pixels, RGB(255,0,0), pipenum)
            self.scene.add_animation(a)
