#!/usr/bin/env python
from __future__ import division

import opc, time

from color import *
import util

numLEDs = 256 
client = opc.Client('localhost:7890')

LEAD  = 1.333 
interp = util.make_interpolater(0, numLEDs, 0, LEAD)

while True:
  for n in range(360):
    c = n / 360
    base = HSV(c, 1.0, 0.666)
    
    pixels = []
    for i in range(numLEDs):
      col = base.copy()
      col.h = (col.h + interp(i)) % 1
      #print col.rgb
      pixels.append(col.rgb)
    client.put_pixels(pixels)
    time.sleep(0.01)
